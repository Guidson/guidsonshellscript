#!/bin/bash
echo "1- HOME: É a variável de ambiente Linux que tem o valor definido como o diretório"
HOME=/home/ifpb
printenv HOME
echo "-------------------------------------------------------------------------"
echo "2- SHELL: descreve o shell que irá interpretar todos os comandos que você digitar. Geralmente, isso será bash por padrão, mas outros valores podem ser definidos caso prefira outras opções."
SHELL=/bin/bash
printenv SHELL
echo "------------------------------------------------------------------------"
echo "3- USER: o usuário que está atualmente conectado."
USER=ifpb 
printenv USER
echo "------------------------------------------------------------------------"
echo "4- PWD: o diretório de trabalho atual."
PWD=/home/ifpb/prova_dia23
printenv PWD
echo "------------------------------------------------------------------------"
echo "5- PATH: uma lista de diretórios que o sistema irá verificar ao procurar por comandos. Quando um usuário digita um comando, o sistema irá verificar diretórios neste pedido para o executável."
echo "------------------------------------------------------------------------"
echo "6- HOSTNAME: o nome de host do computador neste momento."
echo "------------------------------------------------------------------------"
echo "7- _: o comando executado anteriormente mais recente."
echo "------------------------------------------------------------------------"
echo "8- MAIL: o caminho para a caixa de correio do usuário atual."
echo "------------------------------------------------------------------------"
echo "9- TERM: especifica o tipo de terminal a ser emulado ao executar o shell. Diferentes terminais de hardware podem ser emulados para diferentes requisitos de operação. Normalmente, não será necessário se preocupar com isso."
echo "------------------------------------------------------------------------"
echo "10- IFS: o separador de campo interno para separar entradas na linha de comando. Por padrão, é um espaço."
echo "------------------------------------------------------------------------"
echo "11- PS1: a de definição de prompt de comando primário. Isso é usado para definir a aparência do seu prompt quando inicia a sessão do shell. "
echo "------------------------------------------------------------------------"
echo "12- O PS2 é usado para declarar prompts secundários para quando um comando ocupa várias linhas"
echo "------------------------------------------------------------------------"
echo "13 -HISTSIZE: número de linhas de histórico de comando permitidas na memória."
echo "------------------------------------------------------------------------"
echo "14- DIRSTACK: a pilha de diretórios que estão disponíveis com os comandos pushd e popd"
echo "------------------------------------------------------------------------"
echo "15- COLUMNS: o número de colunas de largura que estão sendo usadas para exibir um resultado na tela."





