#!/bin/bash
echo "--------------------------"
echo "Ajuda sobre direcionadores"
echo "--------------------------"
sleep 2

echo "> ou 1> : esse operador serve para redirecionar para saída de um arquivo para o outro"
cat teste1 > teste2
cat teste1
sleep 2
cat teste2
echo "------------------------------------------------------------------------"
echo ">>: esse operador direciona para o fim de um arquivo"
cat teste3 >> teste2
cat teste3
sleep 2
cat teste2
echo "------------------------------------------------------------------------"
echo "<: esse operador direciona para o inicio do arquivo"
cat < teste2
sleep 2
cat teste2
echo "------------------------------------------------------------------------"
echo "<<: esse operador para a entrada e mantém a entrada até que seja digitado algum caractere de EOF (fim do arquivo)"
echo "------------------------------------------------------------------------"
echo ">& ou >2: esse operador direcionha a saida de erro"
cat erro >& result_erro
cat result_erro
echo "------------------------------------------------------------------------"
echo "|: redireciona a saida de um comando para entrada de outro comando"
cat teste2 |ls -l > teste1
cat teste1
echo "------------------------------------------------------------------------"
echo "tee: redireciona o resultado para a saida padrão e para um arquivo, deve ser usado em conjunto ao "|" "
cat teste2 | tee teste1
cat teste1

